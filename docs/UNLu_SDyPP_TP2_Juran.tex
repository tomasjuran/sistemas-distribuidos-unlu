\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[spanish]{babel}
\usepackage{graphicx}
\usepackage{fancyhdr}
\usepackage{multirow}
\usepackage[table]{xcolor}
\usepackage{hyperref}
\usepackage[default]{lato}
\usepackage{listings}
%\usepackage{tabularx}
%\usepackage{layout}

\setkeys{Gin}{width=\linewidth}

\newcommand{\asig}{Sistemas Distribuidos y Programación Paralela}
\newcommand{\codasig}{41409}
\newcommand{\titulo}{Sistemas distribuidos, comunicación y sincronización}

\newcommand{\autor}{Juran, Martín Tomás}
\newcommand{\legajo}{143191}
\newcommand{\email}{tomasjuran96@gmail.com}

%% Header

\headheight = 65pt
\topmargin = -60pt

\fancyhead[L]{%
    \includegraphics[width=50px]{res/logo-unlu.png} \hfill
    \parbox[b]{.7\textwidth}{\flushright
        \asig \space - \codasig \\
        \titulo \\
        \autor \\[5pt]
    }
}

% \newcolumntype{M}[1]{>{\raggedleft\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}

% \fancyhead[L]{%
%     \begin{tabularx}{\textwidth}{m{50px} XM{.7\textwidth}}
%         \includegraphics[width=\linewidth]{res/logo-unlu.png} & &
%         \asig \space - \codasig \newline
%         \titulo \newline
%         \autor 
%     \end{tabularx}
% }

\pagestyle{fancy}

\begin{document}%\layout

%% Carátula

\pagenumbering{gobble}
\begin{titlepage}
    \centering
    \Huge{\titulo}\\[20pt]
    \LARGE{\asig \space - \codasig}\\
    \LARGE{Universidad Nacional de Luján 2019}\\[48pt]
    \includegraphics[width=200px]{res/logo-unlu.png}\\[48pt]
    \vfill
    \LARGE{\autor}\\
    \LARGE{Legajo: \legajo}\\
    \LARGE{\email}
\end{titlepage}

\pagenumbering{arabic}

%% Comienzo del documento

\section*{Comentarios generales}

Todos los programas están preparados para correr ''as-is'', más allá de los parámetros por consola necesarios para definir hosts y puertos. Los ejemplos dados suponen el uso de los archivos de configuración adjuntos a los programas. Ante cualquier dificultad, comunicar sus dudas a \textit{tomasjuran96@gmail.com}.

Todos los programas generan logs a varios niveles (DEBUG, INFO, ERROR), mostrándolos por consola y almacenándolos en el archivo \textit{log.txt} del directorio \textit{logs}. Por esto, para ver los logs de cada programa por separado, es necesario correr cada programa en un directorio distinto. No sucede así con el trabajo de Sobel, donde cada contenedor tiene su propio directorio (volumen).

\section*{Red P2P de carga, búsqueda y descarga de archivos}

Desarrolle una red P2P de carga, búsqueda y descarga de archivos siguiendo las siguientes pautas:

\begin{itemize}
    \item Existen dos tipos de nodos, Maestros y Extremos. Los primeros, son servidores centralizados replicados (al menos 2 nodos) que disponen del listado actualizado de los nodos extremos y se encargan de gestionar la E/S de los peers. Los segundos cumplen dos funciones en el sistema: realizan consultas (como clientes) y atienden solicitudes (como servidores).
    
   \item Cada extremo dispone de un parámetro definido en un archivo de inicialización con las direcciones IP de los nodos Maestros. Al iniciarse se contacta con un maestro el cual funciona como punto de acceso al sistema e informa cuáles son los archivos que dispone para compartir. Luego, está atento a trabajar en dos modos (cliente y servidor)
   
   \begin{itemize}
       \item Como cliente, deriva consultas al nodo maestro y una vez obtenida la respuesta, seleccionará el/los recursos que desee descargar y se contactará con el par correspondiente para descargar el/los archivo/s.
       
       \item Como servidor, recibe la consulta, revisa si matchea la consulta con alguno de los recursos disponibles y devuelve los resultados al nodo que solicitó resultados.
   \end{itemize}

\end{itemize}

A partir de los conceptos vistos en la teoría, critique este modelo y presente mejoras en su propuesta.

\subsection*{Esquema propuesto}

Los masters leen de un archivo de configuración (con \textit{host} y \textit{puerto}) quiénes son los demás masters. El master réplica que se encarga de coordinar la replicación es elegido por su orden en la lista de masters (siempre el primero). Estos mantienen la lista de recursos disponibles que consultan los peers.

Los peers se conectan a alguno de los masters de la lista, eligiendo de manera aleatoria. Estos mantienen una lista local de recursos disponibles, que incluye:

\begin{itemize}
    \item Archivos localizados en el directorio \textit{p2p/Cliente (puerto)}, donde \textit{(puerto)} es el puerto que usa el peer como servidor a otros peers.
    
    \item La lista de recursos disponibles en la red, la cual debe actualizar con una petición al maestro.
\end{itemize}

Es decir, cuando un recurso nuevo se publica en la red, el peer debe pedir la lista actualizada al master, acción que añadirá a su lista local el recurso nuevo.

\subsubsection*{Protocolo de comunicación}

El protocolo definido consta de los siguientes tipos de mensajes:

\begin{itemize}
    \item LIST (peer): un peer envía un mensaje LIST al master asignado para actualizar su lista local de recursos disponibles.
    
    \item POST (peer): un peer envía al master asignado el recurso que quiere publicar.
    
    \item GET (peer): un peer solicita descargar un recurso a otro peer.
    
    \item PUBLISH (master): el master réplica da la orden a los esclavos (se verá más adelante) para publicar un recurso.
    
    \item ACK: mensaje de confirmación. Incluye también la respuesta.
    
    \item ERROR: mensaje de error.
\end{itemize}

Así, el esquema de funcionamiento general es:

\begin{enumerate}
    \item Un peer solicita la lista actualizada al master.
    
    \item El peer recibe la lista actualizada y examina los recursos disponibles.
    
    \item El peer elige un recurso y genera una petición al peer que tiene el recurso.
    
    \item El peer recibe el recurso y lo almacena localmente.
\end{enumerate}

\begin{figure}[!htb]
    \centering
    \includegraphics{res/p2p_main.png}
    \caption{Ejemplo de pedido de la lista actualizada, y luego pedido del recurso a otro peer}
    \label{fig:p2p_main}
\end{figure}

\pagebreak

\subsubsection*{Esquema de Replicación}

Se define un esquema de replicación Maestro-Esclavo, donde se selecciona un master réplica (el primer master en la lista en el archivo de configuración, según su orden) que se encarga de coordinar la replicación de la siguiente manera:

\begin{enumerate}
    \item El master réplica recibe una solicitud de POST de un peer.

    \item Envía a los esclavos (los demás masters de la lista) el recurso nuevo a publicar con un mensaje PUBLISH.
    
    \item Los esclavos responden ACK para informar que recibieron la noticia de publicación y están listos para actualizar su lista de recursos disponibles.
    
    \item Cuando el master réplica recibe el ACK de \textit{todos} los esclavos, envía un ACK para confirmar la publicación y publica el recurso nuevo en su lista.
    
    \item Los esclavos reciben el ACK y publican el recurso. Si estos no pudieran recibir el último mensaje, su lista quedaría inconsistente.
\end{enumerate}

\begin{figure}[!htb]
    \centering
    \includegraphics{res/p2p_post_1.png}
    \caption{Publicación de un nuevo recurso y replicación, cuando el peer envía el POST al master réplica}
    \label{fig:p2p_post_1}
\end{figure}

\pagebreak

Dado este esquema, cuando un peer solicita publicar un nuevo recurso, si el master es esclavo, simplemente lo redirige al master réplica.

\begin{figure}[!htb]
    \centering
    \includegraphics{res/p2p_post_2.png}
    \caption{Publicación de un nuevo recurso y replicación, cuando el peer envía el POST a un master que no es el master réplica}
    \label{fig:p2p_post_2}
\end{figure}

\subsection*{Mejoras propuestas}

\begin{itemize}
    \item Implementar un método de elección de master en el peer, como puede ser por cercanía geográfica o por RTT.
    
    \item Implementar un balanceador de carga, de manera que los recursos sean solicitados al peer con menor carga que disponga del recurso.
    
    \item Implementar un cliente multi-thread que permita descargar un recurso, por partes, de varios peers al mismo tiempo.
    
    \item Permitir cambiar el master réplica de manera dinámica, si el master réplica original se cae.
\end{itemize}

\subsection*{Ejemplo de funcionamiento}

Para probar el programa localmente:

\begin{itemize}
    \item Verificar la existencia del archivo \textit{masters.json} en el directorio \textit{p2p}, desde la raíz del proyecto.
    
    \item Para crear los masters, correr el programa \textit{MainMaster} con los parámetros \textit{host} y \textit{port} correspondientes al master que se quiere iniciar, presente en el archivo de configuración. Hacer esto para cada master en el archivo. Ejemplo:
    
\begin{verbatim}
java -jar MainMaster.jar "127.0.0.1" "8080"
\end{verbatim}
    
    \item Para crear los peers, correr el programa \textit{MainPeer} con los parámetros \textit{host} y \textit{port} que será la dirección que funcionará como servidor para otros peers. Luego se mostrará un menú con las opciones a elegir. Crear una instancia por cada peer que se quiera probar. Ejemplo:
    
\begin{verbatim}
java -jar MainPeer.jar "127.0.0.1" "4040"
\end{verbatim}

\end{itemize}

Cabe aclarar que, para facilidad de testing, si el puerto indicado no está disponible el programa intentará elegir el siguiente (4040 -$>$ 4041), de manera de evitar cambiar los parámetros cada vez que se ejecuta.

\pagebreak

\section*{Sincronización de procesos: Caso banco}

Un banco tiene un proceso para realizar depósitos en cuentas, y otro para extracciones. Ambos procesos corren en simultáneo y aceptan varios clientes a la vez. El proceso que realiza un depósito tarda 40 mseg entre que consulta el saldo actual, y lo actualiza con el nuevo valor. El proceso que realiza una extracción tarda 80 mseg entre que consulta el saldo (y verifica que haya disponible) y lo actualiza con el nuevo valor.

\begin{itemize}
    \item Escribir los dos procesos y realizar pruebas con varios clientes en simultáneo, haciendo operaciones de extracción y depósito. Forzar, y mostrar cómo se logra, errores en el acceso al recurso compartido. El saldo de la cuenta puede ser simplemente un archivo de texto plano.
    
    \item Escribir una segunda versión de los procesos, de forma tal que el acceso al recurso compartido esté sincronizado. Explicar y justificar qué partes se deciden modificar
\end{itemize}

\subsection*{Esquema propuesto}

El banco (clase \textit{Bank}) tiene configurado un retraso fijo entre que consulta el saldo actual y lo actualiza (tanto para depósito como para extracción). En el siguiente ejemplo se realiza un depósito de 1000 y luego una extracción de 1000, antes de que el banco pueda finalizar la primer operación.

\begin{verbatim}
Type an amount to extract or deposit. Example:
+2000 will deposit 2000
-1000 will extract 1000
Type 'sync' to switch between synchronized modes
Type 'exit' to exit
+1000
[pool-1-thread-1] INFO  Balance is 0 before Deposit of 1000
-1000.
[pool-1-thread-2] INFO  Balance is 0 before Extraction of 1000
[pool-1-thread-1] INFO  Balance is 1000 after Deposit of 1000
[pool-1-thread-2] INFO  Balance is -1000 after Extraction of 1000
\end{verbatim}

Se puede ver que, al utilizar como referencia un valor viejo del saldo de la cuenta, la extracción almacenó el valor ''-1000'' en la cuenta, dejando una deuda.

Se decide modificar el programa utilizando \textit{Locks} y
métodos sincronizados.

\pagebreak

\begin{lstlisting}[frame=single, tabsize=2, language=java, stringstyle=\color{blue}, keywordstyle=\color{purple}]
private Object lock = new Object();
...
public void syncDeposit(int amount) {
	synchronized(lock) {
		int lastBalance = balance;
		logger.info("Balance is " + balance +
		" before Synchronized Deposit of " + amount);
		try {
			Thread.sleep(wait);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		balance = lastBalance + amount;
		logger.info("Balance is " + balance +
		" after Synchronized Deposit of " + amount);
	}
}
\end{lstlisting}

El método para extracciones es análogo. Se recomienda también el uso de \textit{ReadWriteLock} de la librería \textit{java.util.concurrent} para usos más avanzados (se utiliza más adelante en la sección ''Balancer'').

\begin{verbatim}
Type an amount to extract or deposit. Example:
+2000 will deposit 2000
-1000 will extract 1000
Type 'sync' to switch between synchronized modes
Type 'exit' to exit
sync
Now working on synchronized mode
+1000
[pool-1-thread-1] INFO  Balance is 0 before Sync Depo of 1000
-1000
[pool-1-thread-1] INFO  Balance is 1000 after Sync Depo of 1000
[pool-1-thread-2] INFO  Balance is 1000 before Sync Extr of 1000
[pool-1-thread-2] INFO  Balance is 0 after Sync Extr of 1000
\end{verbatim}

\subsection*{Ejemplo de funcionamiento}

Ejecutar el programa \textit{MainBankService}, que por defecto comienza en modo asincrónico, y probar con las opciones del menú. No es necesario correr dos programas, ya que la llamada al banco no es bloqueante (sin importar el modo). Simplemente probar realizando una extracción y un depósito antes de que el banco termine la operación anterior.

\pagebreak

\section*{Red dinámica: Balancer}

Construya una red flexible y elástica de nodos (servicios) la cual se adapte (crece / decrece) dependiendo de la carga de trabajo de la misma. El esquema será el de un balanceador de carga y nodos detrás que atienden los pedidos. Para ello deberá implementar mínimamente:

\begin{itemize}
    \item Simulación de carga de nodos. Creación dinámica de conexiones de clientes y pedidos de atención al servicio publicado.
    
    \item Protocolo de sensado para carga general del sistema. Elija un criterio para detectar esa carga y descríbalo. Ejemplo: cantidad de clientes en simultáneo que el servicio puede atender. Si se excede esa cantidad, el punto de entrada (balanceador de carga) crea dinámicamente un nuevo servicio.
    
    \item Definición de umbrales de estado {sin carga, normal, alerta, crítico}
    
    \item Creación, puesta en funcionamiento de los servicios nuevos, y remoción de ellos cuando no sean más necesarios. Para esto el balanceador puede contar con una lista de IPs donde los servicios están instalados y pueden correr. De forma tal que arrancando inicialmente con 2 servicios en 2 nodos distintos, el sistema escala dinámicamente en función de la carga del sistema, usando los nodos listados en ese archivo de configuración. Si fuera necesario, puede haber más de un servicio en un mismo nodo. El servicio debe ser multi thread.
\end{itemize}

\subsection*{Esquema propuesto}

Un cliente solicita un servicio al balancer y este le envía la dirección del nodo que puede responder a ese servicio. El cliente se conecta con el nodo, es atendido y luego se desconecta.

Los servicios disponibles son estáticos, por configuración en el archivo \textit{balancer.json} del directorio \textit{balancer}. También en el archivo de configuración se define la dirección del balancer y de los nodos disponibles.

\subsubsection*{Estado de carga}

Se decide mantener en el balancer el estado de carga de los nodos, para no generar tráfico innecesario sensando a los nodos con sobrecarga. Para la actualización del estado se utiliza sincronización con \textit{Locks}, de manera de mantener la consistencia entre threads. Si se requiere leer el estado de carga, se puede hacer en forma no bloqueante mientras no haya un \textit{Lock} de escritura en efecto (véase \textit{ReadWriteLock} de la librería \textit{java.util.concurrent}).

Los nodos deben enviar mensajes de actualización cuando su nivel de carga cambia. Se decide utilizar el número de conexiones activas con clientes para definir el estado de carga, utilizando los siguientes niveles:

\begin{itemize}
    \item FREE: Sin conexiones activas.
    \item NORMAL: Una conexión activa.
    \item ALERT: Dos conexiones activas.
    \item CRITICAL: Tres conexiones activas.
\end{itemize}

\subsubsection*{Asignación de servicios}

Para la asignación de nodos a un servicio solicitado por el cliente, se sigue el siguiente algoritmo:

\begin{enumerate}
    \item Si existe un nodo activo que puede brindar el servicio y su estado no es ALERT o peor (CRITICAL), se lo elige.
    \item Sino, se intenta agregar un nodo que pueda brindar el servicio y se lo elige.
    \item Si no se pudo agregar un nodo, se busca entre los nodos activos alguno que pueda brindar el servicio, en este caso ignorando si su estado es ALERT (pero no ignorando CRITICAL).
    \item Si todos los nodos que pueden brindar el servicio están en estado CRITICAL y no se pueden agregar más nodos, el balanceador espera a que algún nodo se desocupe (envíe actualización de su estado) y vuelve a 1.
\end{enumerate}

\begin{figure}[!htb]
    \centering
    \includegraphics{res/balancer_algorithm.png}
    \caption{Algoritmo de asignación de servicios del balancer}
    \label{fig:balancer_algorithm}
\end{figure}

Esto significa que nunca se asignará un nodo en estado CRITICAL, y que se pospondrá la asignación a nodos en estado ALERT hasta que no haya más nodos disponibles.

\subsubsection*{Liberación de nodos}

Los nodos envían sus actualizaciones al balancer en cada cambio de estado. Cuando el balancer recibe una actualización de estado FREE, remueve al nodo de la lista de nodos activos.

\pagebreak

\subsection*{Protocolo de comunicación}

El protocolo cuenta con los siguientes tipos de mensaje:

\begin{itemize}
    \item CONNECT (cliente): un cliente solicita un servicio al balancer. Luego, con la dirección recibida, solicita una conexión al nodo correspondiente.
    
    \item STATUS (servicio): un nodo indica al balancer que actualizó su estado.
\end{itemize}

\subsection*{Ejemplo de funcionamiento}

Para probar el programa localmente:

\begin{itemize}
    \item Verificar la existencia del archivo \textit{balancer.json} en el directorio \textit{balancer}, desde la raíz del proyecto.
    
    \item Ejecutar \textit{MainBalancer} que automáticamente leerá su dirección y puerto del archivo de configuración.
    
    \item Ejecutar \textit{MainWorker} para levantar hasta tantos nodos como se hayan configurado. El programa recorrerá automáticamente la lista y levantará los nodos en orden.
    
    \item Ejecutar \textit{MainClient} y solicitar un servicio. El cliente mantendrá una conexión activa con el nodo asignado hasta que se presione \textit{Enter}. De esta manera se puede simular la apropiación de los recursos del nodo, levantando tantos clientes como sean necesarios para forzar la asignación de nodos nuevos.
\end{itemize}

\section*{Procesamiento distribuido: Sobel}

El operador de Sobel es una máscara que, aplicada a una imagen, permite detectar (resaltar) bordes. Este operador es una operación matemática que, aplicada a cada pixel y teniendo en cuenta los pixeles que lo rodean, obtiene un nuevo valor (color) para ese pixel. Aplicando la operación a cada pixel, se obtiene una nueva imagen que resalta los bordes.

\begin{itemize}
    \item Desarrollar un proceso centralizado que tome una imagen, aplique la máscara, y genere un nuevo archivo con el resultado.
    
    \item Desarrolle este proceso de manera distribuida donde se debe partir la imagen en n pedazos, y asignar la tarea de aplicar la máscara a N procesos distribuidos. Después deberá juntar los resultados. Se sugiere implementar los procesos distribuidos usando RMI.
    
    A partir de ambas implementaciones, comente los resultados de performance dependiendo de la cantidad de nodos y tamaño de imagen.
    
    \item Mejore la aplicación del punto anterior para que, en caso de que un proceso distribuido (al que se le asignó parte de la imagen a procesar) se caiga y no responda, el proceso principal detecte esta situación y pida este cálculo a otro proceso.
\end{itemize}

\subsection*{Configuración}

Para este punto es necesario utilizar \textit{docker} y \textit{docker-compose}. Puede encontrar una guía de instalación (y un tutorial) en \url{https://github.com/dpetrocelli/sdypp2020/tree/master/TPS/No\%20obligatorios}.

La configuración depende de varios archivos, todos en el directorio raíz:

\begin{itemize}
    \item \textit{docker-compose.yml}, el archivo de configuración que crea los servicios maestro (broker que distribuye las tareas) y workers.
    
    \item \textit{Dockerfile-master}, el archivo de configuración del broker.
    
    \item \textit{Dockerfile-worker}, el archivo de configuración de los workers.
    
    \item \textit{master.jar}, el ejecutable con el programa del broker.
    
    \item \textit{worker.jar}, el ejecutable con el programa de los workers.
    
    \item \textit{sobel.json}, un archivo que define las direcciones (nombres de servicios de docker-compose) y puertos donde van a operar los workers.
    
    \item \textit{small.png}, una imagen pequeña de prueba.
\end{itemize}

Estos archivos son copiados al contenedor de manera dinámica en cada ejecución, por lo que no es necesario reconstruir la imagen cada vez que se quiere realizar una prueba. Sin embargo, para lograr esto, es necesario crear un volumen (bind mount) del directorio. Esto se puede lograr ejecutando el siguiente comando, ubicado en el directorio raíz donde se encuentran los archivos anteriores.

\begin{verbatim}
docker volume create --driver local \
    -o o=bind \
    -o type=none \
    -o device="$(pwd)" \
    sobel
\end{verbatim}

En bash, ''\$(pwd)'' se resuelve como el directorio actual. Verifique que el comando se ejecutó exitosamente con:

\begin{verbatim}
docker volume inspect sobel
\end{verbatim}

Donde \textit{Options: device:} debería ser el directorio raíz del proyecto.

\subsection*{Prueba}

Para ejecutar una corrida de prueba, que utilizará dos nodos y ejecutará Sobel sobre \textit{small.png}, simplemente correr el siguiente comando en el directorio raíz.

\begin{verbatim}
docker-compose up
\end{verbatim}

Si todo funcionó correctamente, en el directorio raíz se debería haber generado \textit{small.png.sobel.png}, con el filtro aplicado. Ahora puede detener los workers con \textit{CTRL+C}.

\subsection*{Parámetros del programa}

El programa permite modificar distintos parámetros que definen su ejecución. Estos se definen en conjunto con el comando \textit{docker-compose run}, utilizando las variables de entorno con la bandera \textit{-e}.

\begin{itemize}
    \item \textit{img\_file}: nombre de la imagen a procesar, que se debe encontrar en el directorio raíz.

    \item \textit{split}: define cuántos workers utilizar. La configuración de prueba permite utilizar hasta 6 workers. Si se desea agregar más, se debe actualizar el archivo \textit{docker-compose.yml} (se puede copiar y pegar un worker existente y cambiarle el número) y \textit{sobel.json}. Recordar pasar como argumento WORKER y el número que corresponde con la posición en la lista de ese worker.
    
    \item \textit{runs}: cantidad de iteraciones para el cálculo de performance distribuido (se verá más adelante).
\end{itemize}

Por ejemplo, para utilizar dos nodos, no realizar el test de performance y usar la imagen \textit{small.png}, equivalente a la configuración por defecto, utilizar el siguiente comando:

\begin{verbatim}
docker-compose run --rm \
    -e split=2 \
    -e runs=0 \
    -e img_file=small.png \
    master
\end{verbatim}

\subsection*{Tests de performance}

Para realizar tests de performance, cambiar el parámetro \textit{runs} según la sección anterior. Los siguientes resultados corresponden a 100 ejecuciones utilizando una imagen pequeña (404,8 kB; 640x480 px) y una imagen grande (3 MB; 2816x2112 px).

\begin{figure}[!htb]
    \centering
    \includegraphics{res/sobel-small.png}
    \caption{Resultados de 100 ejecuciones de Sobel distribuido con una imagen pequeña}
    \label{fig:sobel-small}
\end{figure}

\begin{figure}[!htb]
    \centering
    \includegraphics{res/sobel-big.png}
    \caption{Resultados de 100 ejecuciones de Sobel distribuido con una imagen grande}
    \label{fig:sobel-big}
\end{figure}

Puede notarse que con la imagen pequeña no se obtienen mejoras al agregar nodos, como es de esperarse, ya que el overhead de dividir la imagen es mayor a la ganancia creada por distribuir el cálculo de Sobel.

En la imagen grande, por otro lado, se encuentran mejoras hasta aproximadamente 5 nodos, donde la curva se atenúa y comienza a crecer nuevamente, por la misma razón que la imagen pequeña.

Para realizar estos tests de performance de manera local, se utilizó la función \textit{cpus} de \textit{docker-compose v2.2}. Esto permite asignar una carga máxima de CPU a los workers. Para utilizar esta función, descomentar las líneas en el archivo \textit{docker-compose.yml}.

Como consecuencia de esto, la performance de realizar el cálculo en forma centralizada siempre sobrepasa a la simulación de varios nodos, pero se espera que con una imagen de tamaño mayor y un cómputo verdaderamente distribuido, se observen resultados diferentes.

\subsection*{Recuperación ante fallas}

Los workers están configurados para recibir como parámetro (con la variable de entorno \textit{fail\_level}) el nivel de falla que debe simular al recibir una tarea. Estos niveles son:

\begin{itemize}
    \item 0: No fallar
    \item 1: Enviar un mensaje incorrecto en lugar del resultado del procesamiento. Luego reanudar el operamiento normal.
    \item 2: Cerrar la conexión con el broker. Luego reanudar el operamiento normal.
    \item 3: Detener el proceso worker (SIGTERM).
\end{itemize}

El broker mantiene una lista de workers trabajando, de manera tal que si uno falla puede revisar la lista de nodos completa, verificar quiénes están disponibles y reasignar la tarea. Para la verificación, envía un PING de aplicación. Si la tarea no puede ser reasignada, el trabajo falla y se aborta el procesamiento.

Para realizar pruebas de falla en los workers, asegurarse que todos estén corriendo:

\begin{verbatim}
docker-compose up -d
\end{verbatim}

Luego, detener el worker que se quiere hacer fallar:

\begin{verbatim}
docker-compose stop worker0
\end{verbatim}

Levantarlo de nuevo con el nivel de falla requerido:

\begin{verbatim}
docker-compose run --rm \
    --use-aliases \
    -e fail_level=3 \
    worker0    
\end{verbatim}

Y finalmente ejecutar el broker:

\begin{verbatim}
docker-compose run --rm --no-deps master
\end{verbatim}

Finalmente, puede revisar los logs de cada uno de los contenedores en el directorio donde docker guarda los volúmenes. En el caso de Ubuntu, este directorio es \textit{/var/lib/docker/volumes}.

\end{document}