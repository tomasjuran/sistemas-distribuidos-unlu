package sdypp.logger;

import java.io.File;

import ch.qos.logback.core.rolling.TriggeringPolicyBase;

public class StartupTriggeringPolicy<E> extends TriggeringPolicyBase<E> {
	private static boolean triggerRollover = true;
	@Override
	public boolean isTriggeringEvent(final File activeFile, final E event) {
		if (!triggerRollover) {
			return false; 
		}
		triggerRollover = false;
		return true;
	}
}
