package sdypp.tp2.balancer;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import sdypp.generic.ConnectionMetadata;
import sdypp.generic.SocketWrapper;
import sdypp.tp2.balancer.balancer.BalancerMessage;
import sdypp.tp2.balancer.balancer.BalancerMessageType;

public class MainClient {
	private static Logger logger = LoggerFactory.getLogger("Client");
	private static ConnectionMetadata balancerMetadata;
	private static Set<String> ServicesAvailable = new HashSet<String>();
	
	public static void main(String[] args) throws IOException {
		Path config = Paths.get("balancer", "balancer.json");
		configClient(Files.newBufferedReader(config));
		
		SocketWrapper balancer;
		Scanner in = new Scanner(System.in);
		System.out.println(Menu());
		String input = in.nextLine().toLowerCase();
		while (!"exit".equals(input)) {
			if (!ServicesAvailable.contains(input)) {
				System.out.println("Service " + input + " not available");
				input = in.nextLine().toLowerCase();
				continue;
			}
			balancer = new SocketWrapper(logger, balancerMetadata.getHost(), balancerMetadata.getPort());
			System.out.println("Asking " + balancer.getRemoteString() + " for service " + input);
			balancer.send(new BalancerMessage(BalancerMessageType.CONNECT, input));
			System.out.println("Sent request to " + balancer.getRemoteString() + ". Awaiting response...");
			
			BalancerMessage msg = (BalancerMessage) balancer.receive();
			balancer.closeSocket();
			
			ConnectionMetadata workerMetadata = (ConnectionMetadata) msg.getMsg();
			System.out.println("Service " + input + " is available @ " + workerMetadata + "\n"
					+ "Connecting...");
			
			SocketWrapper worker = new SocketWrapper(logger, workerMetadata.getHost(), workerMetadata.getPort());
			worker.send(new BalancerMessage(BalancerMessageType.CONNECT, input));
			
			System.out.println("Sent data to " + workerMetadata + "\n"
					+ "Awaiting ACK...");
			worker.receive();
			
			System.out.println("ACK received. Press Enter to release hold on " + workerMetadata);
			in.nextLine();
			worker.send(null);
			
			System.out.println("Disconnected from " + workerMetadata + "...\n");
			System.out.println(Menu());
			input = in.nextLine().toLowerCase();
		}
		in.close();
		System.out.println("Bye");
	}
	
	/**
	 * Reads configuration from file, gets Balancer IP and Port and services available
	 * @param configFile JSON file with configuration
	 * @throws IOException if file could not be read
	 */
	public static void configClient(Reader configFile) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode main = mapper.readTree(configFile);
		
		balancerMetadata = new ConnectionMetadata(
				main.get("balancer").get("host").asText(),
				main.get("balancer").get("port").asInt());
		
		main.get("services").forEach(service -> {
			ServicesAvailable.add(service.asText());
		});
	}
	
	public static String Menu() {
		return "Type the name of the service " + ServicesAvailable + "\n"
				+ "or 'exit' to exit";
	}
}
