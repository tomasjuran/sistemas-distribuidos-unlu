package sdypp.tp2.balancer;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import sdypp.generic.ConnectionMetadata;
import sdypp.generic.Server;
import sdypp.tp2.balancer.balancer.BalancerHandler;
import sdypp.tp2.balancer.worker.WorkerMetadata;

public class MainBalancer {
	private static Logger logger = LoggerFactory.getLogger("Server");
	private static ConnectionMetadata balancer;
	
	public static void main(String[] args) throws IOException {
		Path config = Paths.get("balancer", "balancer.json");
		configBalancer(Files.newBufferedReader(config));
		
		Server server = new Server(logger, balancer.getPort());
		server.setThreadHandlerClass(BalancerHandler.class);
		server.listen();
	}
	
	/**
	 * Reads configuration from file,
	 * gets Balancer IP and Port
	 * sets Balancer's available Service list
	 * sets Balancer's available Workers list
	 * @param configFile JSON file with configuration
	 * @throws IOException if file could not be read
	 */
	public static void configBalancer(Reader configFile) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode main = mapper.readTree(configFile);
		
		balancer = new ConnectionMetadata(
				main.get("balancer").get("host").asText(),
				main.get("balancer").get("port").asInt());
		
		Set<String> servicesAvailableList = new HashSet<String>();
		main.get("services").forEach(service -> {
			servicesAvailableList.add(service.asText());
		});
		BalancerHandler.setServicesAvailable(servicesAvailableList);
		
		List<WorkerMetadata> workersList = new ArrayList<WorkerMetadata>();
		main.get("workers").forEach(worker -> {
			Set<String> serviceList = new HashSet<String>();
			worker.get("services").forEach(x -> serviceList.add(x.asText()));
			workersList.add(new WorkerMetadata(
					worker.get("host").asText(),
					worker.get("port").asInt(),
					serviceList
			));
		});
			
		BalancerHandler.setWorkersAvailable(workersList);
	}
}
