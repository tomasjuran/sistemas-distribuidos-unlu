package sdypp.tp2.balancer.balancer;

public enum BalancerMessageType {
	/**
	 * Client asks for connection
	 */
	CONNECT,
	/**
	 * Ask Worker status
	 */
	STATUS
}