package sdypp.tp2.balancer.balancer;

import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.slf4j.Logger;

import sdypp.generic.ThreadHandler;
import sdypp.tp2.balancer.worker.WorkerMetadata;
import sdypp.tp2.balancer.worker.WorkerStatus;
import sdypp.tp2.balancer.worker.WorkerStatusMessage;

public class BalancerHandler extends ThreadHandler {
	private Logger logger;

	private static final long RETRY_SERVICE = 3000; //ms
	
	private static Set<String> servicesAvailable = null;
	private static List<WorkerMetadata> workersAvailable = null;
	private static Map<WorkerMetadata, WorkerStatus> workersOnJob = null;
	private static ReadWriteLock workerStatusLock = new ReentrantReadWriteLock();
		
	public BalancerHandler(Logger logger, Socket socket, Map<String, Object> args) {
		super(logger, socket, args);
		if (servicesAvailable == null)
			throw new IllegalArgumentException("Balancer requires Services Available list");
		if (workersAvailable == null) 
			throw new IllegalArgumentException("Balancer requires Workers Available list");

		 if (workersOnJob == null)
			 workersOnJob = new HashMap<WorkerMetadata, WorkerStatus>();
		 
		 this.logger = logger;
	}
	
	public static void setServicesAvailable(Set<String> servicesAvailable) {
		BalancerHandler.servicesAvailable = servicesAvailable;
	}

	public static void setWorkersAvailable(List<WorkerMetadata> workersList) {
		BalancerHandler.workersAvailable = workersList;
	}
	
	@Override
	public void run() {
		Object o = receive();
		if (o == null || !(o instanceof BalancerMessage)) {
			logger.error("Received message from " + getRemoteString() +
				" but it was not understood. Message was \"" + o + "\"");
			closeSocket();
			return;
		}
		BalancerMessage msg = (BalancerMessage) o;
		switch (msg.getMsgType()) {
		case CONNECT:
			handleClientConnection(msg);
			break;
		case STATUS:
			handleWorkerUpdate(msg);
			break;
		default:
			logger.error("Received message from " + getRemoteString() +
					" but it was not understood. Message was \"" + msg + "\"");
		}
		closeSocket();
	}
	
	/**
	 * Take connections from clients and redirect to workers
	 * @param service the service the client needs
	 */
	public void handleClientConnection(BalancerMessage msg) {
		String service = (String) msg.getMsg();
		if (!servicesAvailable.contains(service)) {
			logger.error("Client " + getRemoteString() + " requested \"" + service + "\", but it's not available");
			// send error message
			return;
		}
		logger.info("Client " + getRemoteString() + " requested \"" + service + "\"");
		
		WorkerMetadata worker;
		do {
			worker = getAvailableWorker(service, false);
			if (worker == null) {
				// try creating a new worker
				worker = addWorker(service);
				if (worker == null)
					// try getting a worker that's not critical
					worker = getAvailableWorker(service, true);
			}
			if (worker == null)
				// wait until a worker is available
				try {
					logger.info("Could not assign a worker for service \"" + service + "\""
							+ " to client " + getRemoteString() + ". Waiting...");
					Thread.sleep(RETRY_SERVICE);
				} catch (InterruptedException e) {
					e.printStackTrace();
					logger.error("Failed to assign a worker for service \"" + service + "\""
							+ " to client " + getRemoteString());
					return;
				}
		} while (worker == null);
		// send worker info to client
		logger.info("Assigning " + worker + " to handle service \"" + service + "\" for client " + getRemoteString());
		send(new BalancerMessage(BalancerMessageType.CONNECT, worker));
	}
	
	/**
	 * Handle worker status updates
	 * @param update new metadata to update
	 */
	public void handleWorkerUpdate(BalancerMessage msg) {
		WorkerStatusMessage wsm = (WorkerStatusMessage) msg.getMsg();
		logger.info("Received status update " + wsm);
		workerStatusLock.writeLock().lock();
		try {
			if (workersOnJob.containsKey(wsm.metadata)) {
				if (wsm.status == WorkerStatus.FREE) {
				// if worker status is "free", remove from workersOnJob
					workersOnJob.remove(wsm.metadata);
					logger.info("Active workers: " + workersOnJob.size() + ". Removed worker " + wsm.metadata);
				} else {
					WorkerStatus oldStatus = workersOnJob.get(wsm.metadata);
					workersOnJob.put(wsm.metadata, wsm.status);
					logger.info("Updated worker status " + wsm.metadata + " (" + oldStatus + " -> " + wsm.status + ")");
				}
			}
		} finally {
			workerStatusLock.writeLock().unlock();
		}
	}
	
	/**
	 * Query if worker is alive and can run the service
	 * @param service the service to check for
	 * @param ignoreAlert should ask nodes to service anyway if there is no one else available
	 */
	private WorkerMetadata getAvailableWorker(String service, boolean ignoreAlert) {
		WorkerMetadata available = null;
		workerStatusLock.readLock().lock();
		try {
			for (WorkerMetadata worker: workersOnJob.keySet()) {
				if (isWorkerAvailable(workersOnJob.get(worker), ignoreAlert)) {
					available = worker;
					break;
				}
			}
		} finally {
			workerStatusLock.readLock().unlock();
		}
		return available;
	}
	
	private WorkerMetadata addWorker(String service) {
		WorkerMetadata available = null;
		workerStatusLock.readLock().lock();
		try {
			for (WorkerMetadata worker: workersAvailable) {
				if (!workersOnJob.containsKey(worker)
						&& worker.getServices().contains(service)) {
					available = worker;
					break;
				}
			}
		} finally {
			workerStatusLock.readLock().unlock();
		}
		if (available != null) {
			workerStatusLock.writeLock().lock();
			try {
				workersOnJob.put(available, WorkerStatus.FREE);
			} finally {
				workerStatusLock.writeLock().unlock();
			}
			logger.info("Active workers: " + workersOnJob.size() + ". Added new worker " + available);
		}
		return available;
	}
	
	private boolean isWorkerAvailable(WorkerStatus status, boolean ignoreAlert) {
		switch (status) {
		case FREE:
		case NORMAL:
			return true;
		case ALERT:
			if (ignoreAlert)
				return true;
		default:
			return false;
		}
	}
}