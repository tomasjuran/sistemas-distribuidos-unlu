package sdypp.tp2.balancer;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import sdypp.generic.CheckPort;
import sdypp.generic.ConnectionMetadata;
import sdypp.tp2.balancer.worker.Worker;
import sdypp.tp2.balancer.worker.WorkerMetadata;

public class MainWorker {
	private static Logger logger = LoggerFactory.getLogger("Worker");
	public static List<WorkerMetadata> workersAvailable;
	public static ConnectionMetadata balancer;
	
	public static void main(String[] args) throws IOException {
		Path config = Paths.get("balancer", "balancer.json");
		configBalancer(Files.newBufferedReader(config));
		WorkerMetadata metadata = null;
		for (WorkerMetadata worker: workersAvailable) {
			if (CheckPort.IsAvailable(worker.getPort())) {
				metadata = worker;
				break;
			}
		}
		if (metadata == null) {
			System.err.println("No workers available. Check configuration file.");
			return;
		}
		Worker worker = new Worker(logger, balancer, metadata);
		worker.runServer();
	}
	

	/**
	 * Reads configuration from file,
	 * gets Balancer IP and Port
	 * gets available Workers list
	 * @param configFile JSON file with configuration
	 * @throws IOException if file could not be read
	 */
	public static void configBalancer(Reader configFile) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode main = mapper.readTree(configFile);
		
		balancer = new ConnectionMetadata(
				main.get("balancer").get("host").asText(),
				main.get("balancer").get("port").asInt());
		
		workersAvailable = new ArrayList<WorkerMetadata>();
		main.get("workers").forEach(worker -> {
			Set<String> serviceList = new HashSet<String>();
			worker.get("services").forEach(x -> serviceList.add(x.asText()));
			workersAvailable.add(new WorkerMetadata(
					worker.get("host").asText(),
					worker.get("port").asInt(),
					serviceList
			));
		});
	}
	
}
