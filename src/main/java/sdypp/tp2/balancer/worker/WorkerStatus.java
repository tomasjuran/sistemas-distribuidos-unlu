package sdypp.tp2.balancer.worker;

public enum WorkerStatus {
	/**
	 * Has no jobs running
	 */
	FREE,
	/**
	 * Has a job running
	 */
	NORMAL,
	/**
	 * Should not take new jobs
	 */
	ALERT,
	/**
	 * Cannot take new jobs
	 */
	CRITICAL;
	
	public boolean isOnAlert() {
		switch (this) {
		case FREE:
		case NORMAL:
			return false;
		case ALERT:
		case CRITICAL:
		default:
			return true;
		}
	}
}
