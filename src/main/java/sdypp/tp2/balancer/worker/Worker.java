package sdypp.tp2.balancer.worker;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.slf4j.Logger;

import sdypp.generic.ConnectionMetadata;
import sdypp.generic.SocketWrapper;
import sdypp.tp2.balancer.balancer.BalancerMessage;
import sdypp.tp2.balancer.balancer.BalancerMessageType;

public class Worker {
	private Logger logger;
	
	public static final long KEEP_ALIVE = 5000; //ms
	public static final int FREE_THRESHOLD = 0;
	public static final int NORMAL_THRESHOLD = 1;
	public static final int ALERT_THRESHOLD = 2;
	public static final int CRITICAL_THRESHOLD = 3;
	
	private ConnectionMetadata balancer;
	private WorkerMetadata metadata;
	private int aliveConnections = 0;
	private ReadWriteLock aliveLock = new ReentrantReadWriteLock();
	
	public Worker(Logger logger, ConnectionMetadata balancer, WorkerMetadata metadata) {
		this.logger = logger;
		this.balancer = balancer;
		this.metadata = metadata;
	}
	
	public Integer getAliveConnections() {
		aliveLock.readLock().lock();
		Integer result = null;
		try {
			result = aliveConnections;
		} finally {
			aliveLock.readLock().unlock();
		}
		return result;
	}
	
	private void updateAliveConnections(int num) {
		aliveLock.writeLock().lock();
		try {
			aliveConnections += num;
			logger.info("Connections alive: " + aliveConnections);
		} finally {
			aliveLock.writeLock().unlock();
		}
		sendUpdateToBalancer(getAliveConnections());
	}
	
	public void runServer() {
		ServerSocket server = null;
		try {
			server = new ServerSocket(this.metadata.getPort());
			logger.info("Server listening on " + metadata);
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (server == null) {
			logger.error("Server could not start on " + metadata);
			return;
		}
		logger.info("Services available: " + metadata.getServices());
		
		final ServerSocket serverSocket = server;
		ExecutorService serverExecutor = Executors.newSingleThreadExecutor();
		serverExecutor.execute(() -> {
			ExecutorService clientConnectionExecutor = Executors.newCachedThreadPool();
			while (true) {
				try {
					SocketWrapper client = new SocketWrapper(logger, serverSocket.accept());
					updateAliveConnections(1);
					logger.info("Accepted connection from " + client.getRemoteString());
					clientConnectionExecutor.execute(() -> {
						try {
							handleConnection(client);
						} finally {
							updateAliveConnections(-1);
						}
					});
				} catch (IOException e) {
					logger.error("Could not establish connection with client");
					e.printStackTrace();
				}
			}
		});
	}
	
	private void handleConnection(SocketWrapper client) {
		Object o = client.receive();
		if (!(o instanceof BalancerMessage)) {
			logger.error("Received message from " + client.getRemoteString() +
					" but it was not understood. Message was \"" + o + "\"");
			return;
		}
		BalancerMessage msg = (BalancerMessage) o;
		switch (msg.getMsgType()) {
		case CONNECT:
			handleConnectMessage(client, msg);
			break;
		case STATUS:
			handleStatusMessage(client, msg);
			break;
		default:
			logger.error("Received message from " + client.getRemoteString() +
					" but it was not understood. Message was \"" + msg + "\"");
		}
	}
	
	/**
	 * Client trying to request a service
	 * @param msg message sent by the client
	 */
	private void handleConnectMessage(SocketWrapper client, BalancerMessage msg) {
		String service = (String) msg.getMsg();
		logger.info("Received message from " + client.getRemoteString() + " requesting \"" + service + "\"");
		logger.info("Sending response to " + client.getRemoteString() + " for service \"" + service + "\"...");
		client.send(metadata);
		logger.info("Response sent to " + client.getRemoteString() + " for service \"" + service + "\", awaiting ACK...");
		client.receive();
		logger.info("Finished serving \"" + service + "\" to " + client.getRemoteString());
	}
	
	/**
	 * Balancer asking for status update
	 * @param msg message sent by the balancer
	 */
	private void handleStatusMessage(SocketWrapper balancer, BalancerMessage msg) {
		
	}
	
	private void sendUpdateToBalancer(int aliveConnections) {
		WorkerStatus status = WorkerStatus.FREE;
		switch (aliveConnections) {
		case FREE_THRESHOLD:
			break;
		case NORMAL_THRESHOLD:
			status = WorkerStatus.NORMAL;
			break;
		case ALERT_THRESHOLD:
			status = WorkerStatus.ALERT;
			break;
		default:
			status = WorkerStatus.CRITICAL;
		}
		WorkerStatusMessage update = new WorkerStatusMessage(metadata, status);
		BalancerMessage msg = new BalancerMessage(BalancerMessageType.STATUS, update);
		SocketWrapper balancerConnection = new SocketWrapper(logger, balancer.getHost(), balancer.getPort());
		logger.info("Trying to send status update (" + status 
				+ ") to Balancer (" + balancerConnection.getRemoteString() + ")...");
		balancerConnection.send(msg);
		logger.info("Sent status update (" + status 
				+ ") to Balancer (" + balancerConnection.getRemoteString() + ")");
	}
}
