package sdypp.tp2.balancer.worker;

import java.io.Serializable;
import java.util.Set;

import sdypp.generic.ConnectionMetadata;

public class WorkerMetadata extends ConnectionMetadata implements Serializable {
	private static final long serialVersionUID = -5452425134209475821L;
	private final Set<String> services;
	
	public WorkerMetadata(String host, int port, Set<String> services) {
		super(host, port);
		this.services = services;
	}
	
	public Set<String> getServices() {
		return services;
	}
	
	@Override
	public String toString() {
		return super.toString() + " " + services;
	}
}
