package sdypp.tp2.balancer.worker;

import java.io.Serializable;

public class WorkerStatusMessage implements Serializable {
	private static final long serialVersionUID = 3763656927395967624L;
	public final WorkerMetadata metadata;
	public final WorkerStatus status;

	public WorkerStatusMessage(WorkerMetadata metadata, WorkerStatus status) {
		this.metadata = metadata;
		this.status = status;
	}
	
	@Override
	public String toString() {
		return metadata + " " + status;
	}
}
