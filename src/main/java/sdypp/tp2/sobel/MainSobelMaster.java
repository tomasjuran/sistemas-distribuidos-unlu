package sdypp.tp2.sobel;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import sdypp.generic.ConnectionMetadata;
import sdypp.tp2.sobel.master.SobelMaster;

public class MainSobelMaster {
	private static SobelMaster master;
	private static Logger logger = LoggerFactory.getLogger("Server");

	public static void main(String[] args) throws IOException {
		if (args.length < 4) {
			System.out.println("Run program with:");
			System.out.println("java -jar master.jar <config file> <image file> <split> <runs>");
			System.out.println("Example:");
			System.out.println("java -jar master.jar masters.json small.png 2 10");
			System.exit(0);
		}
		
		Path config = Paths.get(args[0]);
		master = new SobelMaster(
				logger,
				getConfig(Files.newBufferedReader(config))
				);
		
		String filename = args[1];
		String extension = filename.substring(filename.lastIndexOf(".") + 1);
		Path path = Paths.get(filename);
		BufferedImage image = ImageIO.read(Files.newInputStream(path));
		
		
		int split = Integer.parseInt(args[2]);
		int runs = Integer.parseInt(args[3]);
		master.setSplit(split);

		if (runs > 0) {
			double[] splitRun = PerformanceTest(image, runs);
			System.out.println("--------------------------------------------------------------------------------");
			System.out.println("Performance test results:");
			System.out.println("-- Distributed run (" + split + " nodes) --");
			showPerformance(splitRun);
		} else {
			try {
				image = master.applySobel(image);
			} catch (SobelAbortException e) {
				System.err.println(e.toString());
				System.exit(0);
			}
			Path sobel = Paths.get(filename + ".sobel." + extension);
			ImageIO.write(image, extension, Files.newOutputStream(sobel));
			System.out.println("Image saved at \"" + sobel + "\"");
		}
	}
	
	public static double[] PerformanceTest(BufferedImage image, int runs) {
		System.out.println("Performance test. Doing " + runs + " runs. Splitting into " + master.getSplit());
		
		// Pre-heat
		System.out.print("Doing pre-heat run");
		for (int r = 0; r < 1; r++) {
			try {
				master.applySobel(image);
			} catch (SobelAbortException e) {
				System.err.println("Image could not be processed. Aborting performance test...");
				System.exit(0);
			}
			System.out.print(".");
		}
		System.out.println("Done!");
		
		// Real runs
		double avgRunTime = 0;
		long maxRunTime = 0;
		long minRunTime = Long.MAX_VALUE;
		
		for (int r = 0; r < runs; r++) {			
			Instant start = Instant.now();
			
			try {
				master.applySobel(image);
			} catch (SobelAbortException e) {
				System.err.println("Image could not be processed. Aborting performance test...");
				System.exit(0);
			}
			
			Instant finish = Instant.now();
			
			long timeElapsed = Duration.between(start, finish).toMillis();
			System.out.println("Run #" + (r+1) + ": " + timeElapsed + " ms");
			
			avgRunTime += timeElapsed;
			if (maxRunTime < timeElapsed) maxRunTime = timeElapsed;
			if (minRunTime > timeElapsed) minRunTime = timeElapsed;
		}
		
		double[] run = new double[3];
		run[0] = avgRunTime / runs;
		run[1] = minRunTime;
		run[2] = maxRunTime;
		return run;
	}
	
	public static void showPerformance(double[] run) {
		System.out.println("Average run time: " + run[0] + " ms");
		System.out.println("Minimum run time: " + run[1] + " ms");
		System.out.println("Maximum run time: " + run[2] + " ms");
	}
	
	/**
	 * Reads configuration from file,
	 * returns Workers list
	 * @param configFile JSON file with configuration
	 * @throws IOException if file could not be read
	 */
	public static List<ConnectionMetadata> getConfig(Reader configFile) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode workers = mapper.readTree(configFile).get("workers");
		
		List<ConnectionMetadata> workersList = new ArrayList<ConnectionMetadata>();
		workers.forEach(worker -> {
			workersList.add(new ConnectionMetadata(
					worker.get("host").asText(),
					worker.get("port").asInt()
			));
		});
		return workersList;
	}
}
