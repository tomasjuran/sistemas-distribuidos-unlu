package sdypp.tp2.sobel.worker;

import java.io.Serializable;

import sdypp.generic.ConnectionMetadata;

public class SobelTask implements Serializable {
	private static final long serialVersionUID = -503065395681902186L;
	public final int fragment;
	public final ConnectionMetadata worker;
	public final int[][] RGBmatrix;
	
	public SobelTask(int fragment, ConnectionMetadata worker, int[][] RGBmatrix) {
		this.fragment = fragment;
		this.worker = worker;
		this.RGBmatrix = RGBmatrix;
	}
}
