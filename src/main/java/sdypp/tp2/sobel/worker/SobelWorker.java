package sdypp.tp2.sobel.worker;

import java.io.IOException;
import java.net.ServerSocket;

import org.slf4j.Logger;

import sdypp.generic.ConnectionMetadata;
import sdypp.generic.SocketWrapper;
import sdypp.tp2.sobel.image.Sobel;
import sdypp.tp2.sobel.master.SobelMessage;
import sdypp.tp2.sobel.master.SobelMessageType;

public class SobelWorker {
	private Logger logger;

	private ConnectionMetadata metadata;
	private int failLevel;

	public SobelWorker(Logger logger, ConnectionMetadata metadata, int failLevel) {
		this.logger = logger;
		this.metadata = metadata;
		this.failLevel = failLevel;
	}
	
	public void runServer() throws IOException {
		try (ServerSocket server = new ServerSocket(this.metadata.getPort())) {
			logger.info("Server listening on " + metadata);
			while (true) {
				SocketWrapper master = new SocketWrapper(logger, server.accept());
				logger.info("Accepted connection from master " + master.getRemoteString());
				
				Object o = master.receive();
				if (o == null || !(o instanceof SobelMessage)) {
					logger.error("Received message from " + master.getRemoteString() +
						" but it was not understood. Message was \"" + o + "\"");
					master.closeSocket();
					return;
				}
				SobelMessage msg = (SobelMessage) o;
				switch (msg.type) {
				case PING:
					logger.info("Received PING from master " + master.getRemoteString());
					master.send(new SobelMessage(SobelMessageType.PING, null));
					break;
				case SUBMIT:
					handleSubmitTask(master, msg.task);
					break;
				default:
					logger.error("Received message from " + master.getRemoteString() +
							" but it was not understood. Message was \"" + msg + "\"");
				}
				master.closeSocket();
			}
		}
	}
	
	private void handleSubmitTask(SocketWrapper master, SobelTask task) {
		logger.info("Received fragment " + task.fragment + ""
				+ " from master " + master.getRemoteString() + ". Applying Sobel...");
		
		switch (failLevel) {
		case 0:
			break;
		case 1:
			logger.debug("Fail level 1: Attempting to send invalid message");
			master.send(null);
			failLevel = 0;
			break;
		case 2:
			logger.debug("Fail level 2: Attempting to close connection without sending message");
			failLevel = 0;
			return;
		case 3:
			logger.debug("Fail level 3: Attempting to end worker (SIGTERM)");
			System.exit(0);
		}
		
		int[][] RGBmatrix = Sobel.Calculate(task.RGBmatrix);
		logger.info("Applied Sobel to fragment " + task.fragment + "."
				+ " Sending back to master " + master.getRemoteString() + "...");
		
		SobelTask completedTask = new SobelTask(task.fragment, this.metadata, RGBmatrix);
		master.send(new SobelMessage(SobelMessageType.SUBMIT, completedTask));
		logger.info("Fragment " + task.fragment + " sent to master " + master.getRemoteString());
	}
}
