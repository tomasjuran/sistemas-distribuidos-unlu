package sdypp.tp2.sobel;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import sdypp.generic.ConnectionMetadata;
import sdypp.tp2.sobel.worker.SobelWorker;

public class MainSobelWorker {
	private static Logger logger = LoggerFactory.getLogger("Worker");
	
	public static void main(String[] args) throws IOException {
		if (args.length < 2) {
			System.out.println("Run program with:");
			System.out.println("java -jar worker.jar <config file> <worker#> [failLevel]");
			System.out.println("Example:");
			System.out.println("java -jar worker.jar sobel.json 0 0");
			System.exit(0);
		}
		Path config = Paths.get(args[0]);
		List<ConnectionMetadata> availableWorkers = getConfig(Files.newBufferedReader(config));
		
		ConnectionMetadata metadata;
		try {
			metadata = availableWorkers.get(Integer.parseInt(args[1]));
		} catch (Exception e) {
			System.err.println("Worker #" + args[1] + " is not available. Check configuration file.");
			return;
		}
		
		int failLevel = 0;
		if (args.length > 2)
			failLevel = Integer.parseInt(args[2]);
		SobelWorker worker = new SobelWorker(logger, metadata, failLevel);
		worker.runServer();
	}
	
	/**
	 * Reads configuration from file,
	 * returns Workers list
	 * @param configFile JSON file with configuration
	 * @throws IOException if file could not be read
	 */
	public static List<ConnectionMetadata> getConfig(Reader configFile) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode workers = mapper.readTree(configFile).get("workers");
		
		List<ConnectionMetadata> workersList = new ArrayList<ConnectionMetadata>();
		workers.forEach(worker -> {
			workersList.add(new ConnectionMetadata(
					worker.get("host").asText(),
					worker.get("port").asInt()
			));
		});
		return workersList;
	}
}
