package sdypp.tp2.sobel.image;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class SobelImage {
	public static int[][] getRGBmatrix(BufferedImage image) {
		final int x = image.getWidth();
		final int y = image.getHeight();
		int[][] RGBmatrix = new int[y][x];
		for (int i = 0; i < y; i++) {
			for (int j = 0; j < x; j++) {
				RGBmatrix[i][j] = getGrayScale(image.getRGB(j, i));
			}
		}
		return RGBmatrix;
	}
	
	public static void setRGBmatrix(BufferedImage image, int[][] RGBmatrix) {
		final int x = image.getWidth();
		final int y = image.getHeight();
		for (int i = 0; i < y; i++) {
			for (int j = 0; j < x; j++) {
				image.setRGB(j, i, RGBmatrix[i][j]);
			}
		}
	}
	
	public static BufferedImage copyImage(BufferedImage image) {
		BufferedImage copy = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
	    Graphics g = copy.getGraphics();
	    g.drawImage(image, 0, 0, null);
	    g.dispose();
	    return copy;
	}
	
	public static int getGrayScale(int rgb) {
	    int r = (rgb >> 16) & 0xff;
	    int g = (rgb >> 8) & 0xff;
	    int b = (rgb) & 0xff;
	    //from https://en.wikipedia.org/wiki/Grayscale, calculating luminance
	    int gray = (int)(0.2126 * r + 0.7152 * g + 0.0722 * b);
	    return gray;
    }
	
	public static BufferedImage[] splitImage(BufferedImage image, int split) {
		BufferedImage[] fragments = new BufferedImage[split];
		
		return fragments;
	}
}
