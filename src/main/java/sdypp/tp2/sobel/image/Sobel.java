package sdypp.tp2.sobel.image;

import sdypp.utils.Matrix;

public class Sobel {
	
	public static final int[][] Gx = {
			{ 1,  0, -1},
			{ 2,  0, -2},
			{ 1,  0, -1}
	};
	
	public static final int[][] Gy = {
			{ 1,  2,  1},
			{ 0,  0,  0},
			{-1, -2, -1}
	};
	
	public static int CalculatePoint(int[][] matrix) {
		int gx = Matrix.Convolution(Gx, matrix);
		int gy = Matrix.Convolution(Gy, matrix);
		double g = Math.sqrt((gx*gx) + (gy*gy));
		return (int) g;
	}
	
	public static int[][] Calculate(int[][] matrix) {
		int rows = matrix.length;
		int cols = matrix[0].length;
		int maxGradient = -1;
		int[][] result = new int[rows][cols];
		for (int i = 1; i < rows-1; i++) {
			for (int j = 1; j < cols-1; j++) {
				int[][] surroundingPoints = {
						{matrix[i-1][j-1], matrix[i-1][j], matrix[i-1][j+1]},
						{matrix[i][j-1], matrix[i][j], matrix[i][j+1]},
						{matrix[i+1][j-1], matrix[i+1][j], matrix[i+1][j+1]}
				};
				int g = CalculatePoint(surroundingPoints);
				result[i][j] = g;
				if (g > maxGradient) maxGradient = g;
			}
		}
		double scale = 255.0 / maxGradient;
		for (int i = 1; i < rows-1; i++) {
			for (int j = 1; j < cols-1; j++) {
				int scaledG = result[i][j];
				scaledG = (int)(scaledG * scale);
				scaledG = 0xff000000 | (scaledG << 16) | (scaledG << 8) | scaledG;
				result[i][j] = scaledG;
			}
		}
		return result;
	}
}
