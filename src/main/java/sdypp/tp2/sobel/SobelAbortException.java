package sdypp.tp2.sobel;

public class SobelAbortException extends Exception {
	private static final long serialVersionUID = 5398355613189537980L;
	
	public SobelAbortException(String message) {
		super(message);
	}
}
