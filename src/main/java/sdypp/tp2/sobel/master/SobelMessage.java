package sdypp.tp2.sobel.master;

import java.io.Serializable;

import sdypp.tp2.sobel.worker.SobelTask;

public class SobelMessage implements Serializable {
	private static final long serialVersionUID = -2153109929035642349L;
	public final SobelMessageType type;
	public final SobelTask task;
	public SobelMessage(SobelMessageType type, SobelTask task) {
		this.type = type;
		this.task = task;
	}
}
