package sdypp.tp2.sobel.master;

public enum SobelMessageType {
	/**
	 * Query if worker is alive
	 */
	PING,
	/**
	 * Submit a task
	 */
	SUBMIT
}
