package sdypp.tp2.sobel.master;

import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.slf4j.Logger;

import sdypp.generic.ConnectionMetadata;
import sdypp.generic.SocketWrapper;
import sdypp.tp2.sobel.SobelAbortException;
import sdypp.tp2.sobel.image.Sobel;
import sdypp.tp2.sobel.image.SobelImage;
import sdypp.tp2.sobel.worker.SobelTask;

public class SobelMaster {
	private Logger logger;
	private int split = 1;
	private List<ConnectionMetadata> workersList;
	private Set<ConnectionMetadata> workersOnJob = new HashSet<ConnectionMetadata>();
	
	public int getSplit() {
		return split;
	}

	public void setSplit(int split) {
		this.split = split;
	}

	public SobelMaster(Logger logger, List<ConnectionMetadata> workersList) {
		this.logger = logger;
		this.workersList = workersList;
	}

	public BufferedImage applySobel(BufferedImage image) throws SobelAbortException {
		return applySobelDistributed(image);
	}
	
	@SuppressWarnings("unused")
	private BufferedImage applySobelSingle(BufferedImage image) {
		int[][] RGBmatrix = SobelImage.getRGBmatrix(image);
		RGBmatrix = Sobel.Calculate(RGBmatrix);
		SobelImage.setRGBmatrix(image, RGBmatrix);
		return image;
	}
	
	private BufferedImage applySobelDistributed(BufferedImage image) throws SobelAbortException {
		logger.info("Spliting image into " + split + " fragments.");
		int[][] RGBmatrix = SobelImage.getRGBmatrix(image);
		int[][][] fragments = splitMatrix(RGBmatrix, split);
		
		ExecutorService executor = Executors.newFixedThreadPool(split);
		CompletionService<SobelTask> completionService = new ExecutorCompletionService<SobelTask>(executor);
		
		for (int i = 0; i < split; i++) {
			int[][] fragmentMatrix = fragments[i];
			// distribute work
			ConnectionMetadata workerMetadata = workersList.get(i);
			final int fragment = i;
			completionService.submit(new SobelSubmitTask(
					logger,
					fragment,
					workerMetadata,
					fragmentMatrix)
			);
			workersOnJob.add(workerMetadata);
		}
		// end for

		int[][][] completedFragments = new int[split][][];		
		int completed = 0;
		boolean errors = false;
		
		while(completed < split && !errors) {
			try {
				Future<SobelTask> resultFuture = completionService.take();
				SobelTask result = resultFuture.get();
				workersOnJob.remove(result.worker);
				if (result.RGBmatrix != null) {
					completed++;
					completedFragments[result.fragment] = result.RGBmatrix;
				} else {
					// retask
					logger.info("Fragment " + result.fragment + " failed. Retasking...");
					ConnectionMetadata availableWorkerMetadata = null;
					for (ConnectionMetadata workerMetadata: workersList) {
						if (workersOnJob.contains(workerMetadata))
							continue;
						// worker available
						SocketWrapper worker = new SocketWrapper(
								logger,
								workerMetadata.getHost(),
								workerMetadata.getPort()
						);
						if (worker.connectionIsAlive()) {
							worker.send(new SobelMessage(SobelMessageType.PING, null));
							logger.info("Sent PING to worker " + workerMetadata);
							worker.receive();
							logger.info("Received PING from worker " + workerMetadata);
							availableWorkerMetadata = workerMetadata;
							break;
						}
					}
					
					if (availableWorkerMetadata == null) {
						logger.error("No workers available for fragment " + result.fragment + "."
								+ "Aborting job...");
						errors = true;
					} else {
						// submit new task
						completionService.submit(new SobelSubmitTask(
								logger,
								result.fragment,
								availableWorkerMetadata,
								fragments[result.fragment])
						);
					}
				}
			} catch (InterruptedException | ExecutionException e) {
				errors = true;
			}
		}
		executor.shutdownNow();
		
		if (errors) {
			logger.error("Job could not be completed. Job aborted");
			throw new SobelAbortException("Job could not be completed. Job aborted");
		}
		
		logger.info("Job completed successfully. Joining fragments...");
		int[][] sobelRGBmatrix = joinMatrix(completedFragments);
		SobelImage.setRGBmatrix(image, sobelRGBmatrix);
		logger.info("Sobel applied successfully!");
		return image;
	}
	
	private int[][][] splitMatrix(int[][] RGBmatrix, int split) {
		int[][][] fragments = new int[split][][];
		int rowStart = 0;
		int rowsPerFragment = RGBmatrix.length / split;
		int rowEnd = rowsPerFragment + 1;
		for (int fragment = 0; fragment < split-1; fragment++) {
			fragments[fragment] = Arrays.copyOfRange(RGBmatrix, rowStart, rowEnd);
			// add row before (extra -1)
			rowStart = rowEnd - 2;
			// and row after (extra +1)
			rowEnd = rowStart + rowsPerFragment + 1;
		}
		// last fragment ends exactly on image
		fragments[split-1] = Arrays.copyOfRange(RGBmatrix, rowStart, RGBmatrix.length);
		return fragments;
	}
	
	private int[][] joinMatrix(int[][][] superMatrix) {
		// first and last row
		int finalLength = 2;
		for (int[][] subMatrix: superMatrix) {
			// ignore extra top and bottom rows used to calculate surrounding pixels
			finalLength += subMatrix.length - 2;
		}
		int[][] result = new int[finalLength][];
		// add first "empty" row
		result[0] = superMatrix[0][0];
		int elementsCopied = 1;
		for (int[][] subMatrix: superMatrix) {
			// ignore extra top and bottom rows used to calculate surrounding pixels
			System.arraycopy(subMatrix, 1, result, elementsCopied, subMatrix.length-2);
			elementsCopied += subMatrix.length-2;
		}
		// add last row
		int[][] lastSubMatrix = superMatrix[superMatrix.length-1];
		result[result.length-1] = lastSubMatrix[lastSubMatrix.length-1];
		return result;
	}
}
