package sdypp.tp2.sobel.master;

import java.io.IOException;
import java.util.concurrent.Callable;

import org.slf4j.Logger;

import sdypp.generic.ConnectionMetadata;
import sdypp.generic.SocketWrapper;
import sdypp.tp2.sobel.worker.SobelTask;

public class SobelSubmitTask implements Callable<SobelTask> {
	private Logger logger;
	private int fragment;
	private ConnectionMetadata workerMetadata;
	private int[][] RGBmatrix;

	public SobelSubmitTask(Logger logger, int fragment, ConnectionMetadata workerMetadata, int[][] RGBmatrix) {
		this.logger = logger;
		this.fragment = fragment;
		this.workerMetadata = workerMetadata;
		this.RGBmatrix = RGBmatrix;
	}
	
	@Override
	public SobelTask call() throws Exception {
		try {
			logger.info("Attempting connection to " + workerMetadata + "...");
			SocketWrapper worker = new SocketWrapper(logger, workerMetadata.getHost(), workerMetadata.getPort());
			if (!worker.connectionIsAlive())
				throw new IOException("Worker " + workerMetadata + " is not available");
			logger.info("Sending fragment " + fragment + " to worker " + workerMetadata + "...");
			SobelTask task = new SobelTask(fragment, workerMetadata, RGBmatrix);
			worker.send(new SobelMessage(SobelMessageType.SUBMIT, task));
			logger.info("Sent fragment " + fragment + " to worker " + workerMetadata + "."
					+ " Awaiting response...");
			Object o = worker.receive();
			if (o == null || !(o instanceof SobelMessage))
				throw new IOException("Received [" + o + "]. Expected completed SobelTask");
			SobelTask completed = ((SobelMessage) o).task;
			logger.info("Received fragment " + fragment + " from worker " + workerMetadata + "."
					+ " Task complete!");
			return completed;
		} catch (Exception e) {
			logger.debug(e.toString());
		}
		logger.error("Worker " + workerMetadata + " could not finish fragment " + fragment + "."
				+ " Aborting...");
		return new SobelTask(fragment, workerMetadata, null);
	}

}
