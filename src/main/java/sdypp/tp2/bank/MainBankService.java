package sdypp.tp2.bank;

import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainBankService {
	public static void main(String[] args) {
		Bank bank = new Bank();
		ExecutorService e = Executors.newCachedThreadPool();
		boolean sync = false;
		System.out.println(Menu());
		Scanner in = new Scanner(System.in);
		String input = in.nextLine().toLowerCase();
		while (!input.equals("exit")) {
			if (input.equals("sync")) {
				sync = !sync;
				if (sync)
					System.out.println("Now working on synchronized mode");
				else
					System.out.println("Now working on asynchronous mode");
			} else {
				int amount = Integer.parseInt(input);
				if (amount > 0) {
					if (sync)
						e.execute(() -> {
							bank.syncDeposit(amount);
						});
					else
						e.execute(() -> {
							bank.deposit(amount);
						});
				} else {
					if (sync)
						e.execute(() -> {
							bank.syncExtraction(-amount);
						});
					else
						e.execute(() -> {
							bank.extraction(-amount);
						});
				}
			}
			input = in.nextLine().toLowerCase();
		}
		in.close();
	}
	
	public static String Menu() {
		return "Type an amount to extract or deposit. Example:\n"
				+ "+2000 will deposit 2000\n"
				+ "-1000 will extract 1000\n"
				+ "Type 'sync' to switch between synchronized modes\n"
				+ "Type 'exit' to exit";
	}
}
