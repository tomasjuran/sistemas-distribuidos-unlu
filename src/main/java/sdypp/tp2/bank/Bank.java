package sdypp.tp2.bank;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Bank {
	private Logger logger = LoggerFactory.getLogger(Bank.class);
	public int wait = 3000;
	private int balance = 0;
	private Object lock = new Object();
	
	public void deposit(int amount) {
		int lastBalance = balance;
		logger.info("Balance is " + balance + " before Deposit of " + amount);
		try {
			Thread.sleep(wait);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		balance = lastBalance + amount;
		logger.info("Balance is " + balance + " after Deposit of " + amount);
	}
	
	public void extraction(int amount) {
		int lastBalance = balance;
		logger.info("Balance is " + balance + " before Extraction of " + amount);
		try {
			Thread.sleep(wait);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		balance = lastBalance - amount;
		logger.info("Balance is " + balance + " after Extraction of " + amount);
	}
	
	public void syncExtraction(int amount) {
		synchronized(lock) {
			int lastBalance = balance;
			logger.info("Balance is " + balance + " before Synchronized Extraction of " + amount);
			try {
				Thread.sleep(wait);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			balance = lastBalance - amount;
			logger.info("Balance is " + balance + " after Synchronized Extraction of " + amount);
		}
	}
	
	public void syncDeposit(int amount) {
		synchronized(lock) {
			int lastBalance = balance;
			logger.info("Balance is " + balance + " before Synchronized Deposit of " + amount);
			try {
				Thread.sleep(wait);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			balance = lastBalance + amount;
			logger.info("Balance is " + balance + " after Synchronized Deposit of " + amount);
		}
	}
}
