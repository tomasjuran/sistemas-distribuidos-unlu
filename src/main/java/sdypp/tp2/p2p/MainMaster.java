package sdypp.tp2.p2p;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import sdypp.generic.CheckPort;
import sdypp.generic.ConnectionMetadata;
import sdypp.generic.Server;
import sdypp.tp2.p2p.master.P2PMasterHandler;

public class MainMaster {
	private static Logger logger = LoggerFactory.getLogger("Server");
	
	public static void main(String[] args) {
		try {
			File configFile = new File("p2p", "masters.json");
			configP2PMasterHandler(configFile);
			String host = args[0];
			int port = CheckPort.GetAvailable(Integer.parseInt(args[1]));
			Server server = new Server(logger, port);
			
			ConnectionMetadata serverMetadata = new ConnectionMetadata(host, port);
			P2PMasterHandler.setMetadata(serverMetadata);
			
			server.setThreadHandlerClass(P2PMasterHandler.class);
			server.listen();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Gets masters list and replica master from json configuration file,
	 * and sets those values on {@code P2PMasterHandler}.
	 * @param configFile json file with configuration
	 * @throws IOException if file could not be read
	 */
	public static void configP2PMasterHandler(File configFile) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode masters = mapper.readTree(configFile).get("masters");
		// First master chosen as Replica Master
		ConnectionMetadata replicaMaster = new ConnectionMetadata(
				masters.get(0).get("host").asText(),
				masters.get(0).get("port").asInt());
		P2PMasterHandler.setReplicaMasterMetadata(replicaMaster);
		
		List<ConnectionMetadata> masterList = new ArrayList<ConnectionMetadata>();
		masterList.add(replicaMaster);
		
		int length = masters.size();
		for (int i = 1; i < length; i++) {
			JsonNode master = masters.get(i);
			masterList.add(new ConnectionMetadata(
					master.get("host").asText(),
					master.get("port").asInt()));
		}
		P2PMasterHandler.setMasterList(masterList);
	}
}