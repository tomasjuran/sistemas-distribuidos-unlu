package sdypp.tp2.p2p.master;

import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.slf4j.Logger;

import sdypp.generic.ConnectionMetadata;
import sdypp.generic.SocketWrapper;
import sdypp.generic.ThreadHandler;
import sdypp.tp2.p2p.P2PMessage;
import sdypp.tp2.p2p.P2PMessageType;

public class P2PMasterHandler extends ThreadHandler {
	private static List<ConnectionMetadata> masterList = null;
	private static ConnectionMetadata replicaMasterMetadata = null;
	private static ConnectionMetadata metadata = null;
	
	private static ConcurrentMap<String, String> resourcesList = null;
	
	public static List<ConnectionMetadata> getMasterList() {
		return masterList;
	}

	public static ConnectionMetadata getReplicaMasterMetadata() {
		return replicaMasterMetadata;
	}

	public static ConnectionMetadata getMetadata() {
		return metadata;
	}

	public static ConcurrentMap<String, String> getResourcesList() {
		return resourcesList;
	}

	public static void setMasterList(List<ConnectionMetadata> masterList) {
		P2PMasterHandler.masterList = masterList;
	}

	public static void setReplicaMasterMetadata(ConnectionMetadata replicaMasterMetadata) {
		P2PMasterHandler.replicaMasterMetadata = replicaMasterMetadata;
	}

	public static void setMetadata(ConnectionMetadata metadata) {
		P2PMasterHandler.metadata = metadata;
	}

	public P2PMasterHandler(Logger logger, Socket socket, Map<String, Object> args) {
		// Client socket is assigned in super()
		super(logger, socket, args);
		if (masterList == null || metadata == null || replicaMasterMetadata == null) {
			String error = "Configuration could not be loaded (check masters configuration file)";
			logger.error(error);
			throw new IllegalArgumentException(error);
		}
		if (resourcesList == null) {
			resourcesList = new ConcurrentHashMap<String, String>();
		}
	}

	@Override
	public void run() {
		while(connectionIsAlive()) {
			Object o = receive();
			if (o != null && o instanceof P2PMessage) {
				P2PMessage msg = (P2PMessage) o;
				switch(msg.getMsgType()) {
				// Client messages
				case LIST:
					handleList(msg);
					break;
				case POST:
					handlePost(msg);
					break;
				// Master messages
				case PUBLISH:
					handlePublish(msg);
					break;
				default:
					logger.error("Received message from " + getRemoteString() +
							" but it was not understood. Message was \"" + msg + "\"");
					break;
				}
			}
		}
	}
	
	public void handleList(P2PMessage msg) {
		logReceived(msg);
		Map<String, String> resourcesList = new HashMap<String, String>(getResourcesList());
		P2PMessage response = new P2PMessage(P2PMessageType.LIST, resourcesList);
		send(response);
		logSent(response);
	}
	
	@SuppressWarnings("unchecked")
	public void handlePost(P2PMessage msg) {
		logReceived(msg);
		if (amIReplicaMaster()) {
			// Publish and save
			Object received = msg.getMsg();
			if (received instanceof Map) {
				Map<String, String> resourceMap = (Map<String, String>) received;
				publishToMasters(new P2PMessage(P2PMessageType.PUBLISH, resourceMap));
			} else {
				logger.error("Received message from " + getRemoteString() + ". Expected Resource + Location, received " + msg);
			}
		} else { // if (!amIReplicaMaster())
			// Forward to Replica Master
			logger.debug("Received " + msg.getMsgType() + ", but I am not Replica Master!");
			P2PMessage post = new P2PMessage(P2PMessageType.POST, msg.getMsg());
			SocketWrapper replicaMasterSocket = connectToReplicaMaster();
			replicaMasterSocket.send(post);
			logger.info("Sent " + msg.getMsgType() + " to Replica Master (" + replicaMasterSocket.getRemoteString() + ")");
		}
	}
	
	@SuppressWarnings("unchecked")
	public void handlePublish(P2PMessage msg) {
		logReceived(msg);
		if (hostIsTrusted()) {
			Map<String, String> resourceMap = (Map<String, String>) msg.getMsg();
			send(new P2PMessage(P2PMessageType.ACK, null));
			logger.info("Sent " + P2PMessageType.ACK +
					" in response to " + P2PMessageType.PUBLISH +
					" request, with message \"" + msg +
					"\" to Replica Master (" + getRemoteString() + ")");
			P2PMessage ack = (P2PMessage) receive();
			if (ack.getMsgType() == P2PMessageType.ACK) {
				logger.info(P2PMessageType.ACK + " received, to " +
						P2PMessageType.PUBLISH + " resource \"" +
						msg + "\" from Replica Master (" + getRemoteString() + ")");
				addResource(resourceMap);
			}
		} else {
			logger.error("Non-trusted host " + getRemoteString() + " tried to publish a resource!");
		}
	}
	
	@SuppressWarnings("unchecked")
	private void publishToMasters(P2PMessage publishMsg) {
		// Don't include myself
		int nMasters = getMasterList().size() - 1;
		if (nMasters < 1) {
			Map<String, String> resourceMap = (Map<String, String>) publishMsg.getMsg();
			addResource(resourceMap);
			logger.info("I am the only master. Added resource " + resourceMap + " successfully");
			return;
		}
		// Publish to all slaves
		logger.info("Preparing to send \"" + publishMsg + "\" to all Masters");
		ExecutorService executorPublish = Executors.newFixedThreadPool(nMasters);
		CompletionService<Boolean> completionService = new ExecutorCompletionService<Boolean>(executorPublish);
		List<SocketWrapper> masterSocketList = new ArrayList<SocketWrapper>();
		for (int i = 0; i <= nMasters; i++) {
			ConnectionMetadata master = getMasterList().get(i);
			// Don't send to myself!
			if (!master.equals(metadata)) {
				SocketWrapper socket = new SocketWrapper(logger, master.getHost(), master.getPort());
				masterSocketList.add(socket);
			}
		}
		// Ask all masters to publish
		for (SocketWrapper masterConnection: masterSocketList) {
			completionService.submit(() -> {
				logger.info("Trying to ask " + masterConnection.getRemoteString() + " to publish \"" + publishMsg + "\"");
				if (!masterConnection.send(publishMsg)) {
					logger.error("Asking " + masterConnection.getRemoteString() + " to publish \"" + publishMsg + "\" failed. Aborting publish...");
					return false;
				}
				logger.info(masterConnection.getRemoteString() + " was asked to publish \"" + publishMsg + "\". Waiting for ACK...");
				Object ack = masterConnection.receive();
				if (ack == null || !(ack instanceof P2PMessage)) {
					logger.error(masterConnection.getRemoteString() + " did not send ACK for publishing \"" + publishMsg + "\". Aborting publish...");
					return false;
				}
				P2PMessage ackMsg = (P2PMessage) ack;
				if (ackMsg.getMsgType() != P2PMessageType.ACK) {
					logger.error(masterConnection.getRemoteString() + " did not send ACK for publishing \"" + publishMsg + "\". Aborting publish...");
					return false;
				} else {
					logger.info(masterConnection.getRemoteString() + " sent ACK for publishing \"" + publishMsg + "\"");
					return true;
				}
			});
		} 
		// end for
		int received = 0;
		boolean errors = false;
		while(received < nMasters && !errors) {
			try {
				Future<Boolean> resultFuture = completionService.take();
				Boolean result = resultFuture.get();
				if (result) {
					received++;
				} else {
					errors = true;
				}
			} catch (InterruptedException | ExecutionException e) {
				errors = true;
			}
		}
		executorPublish.shutdownNow();
		if (errors) {
			logger.error("Publishing \"" + publishMsg + "\" failed.");
		} else {
			logger.info("Publishing \"" + publishMsg + "\": ACK received, sending responses...");
			ExecutorService executorACK = Executors.newFixedThreadPool(nMasters);
			for (SocketWrapper masterConnection: masterSocketList) {
				executorACK.execute(() -> {
					if (masterConnection.send(new P2PMessage(P2PMessageType.ACK, null))) {
						logger.info(masterConnection.getRemoteString() + " was sent ACK to publish \"" +
								publishMsg + "\"");
					} else {
						logger.error(masterConnection.getRemoteString() + " did not receive ACK to publish \"" +
								publishMsg + "\". Masters may be out of sync!");
					}
				});
			}
			addResource((Map<String, String>) publishMsg.getMsg());
			executorACK.shutdown();
		}
	}
	
	private void addResource(Map<String, String> resourceMap) {
		P2PMasterHandler.getResourcesList().putAll(resourceMap);
	}
	
	private boolean amIReplicaMaster() {
		return P2PMasterHandler.getMetadata().equals(P2PMasterHandler.getReplicaMasterMetadata());
	}
	
	private SocketWrapper connectToReplicaMaster() {
		return new SocketWrapper(
				logger,
				P2PMasterHandler.replicaMasterMetadata.getHost(),
				P2PMasterHandler.replicaMasterMetadata.getPort());
	}
	
	/**
	 * @return true if host is Replica Master
	 */
	private boolean hostIsTrusted() {
		return true;
		// return getReplicaMasterMetadata().equals(new MasterMetadata(this.socket));
	}
	
	private void logReceived(P2PMessage msg) {
		logger.info(getRemoteString() + " sent \"" + msg + "\"");
	}
	
	private void logSent(P2PMessage msg) {
		logger.info(getRemoteString() + " was sent the message \"" + msg + "\"");
	}

}
