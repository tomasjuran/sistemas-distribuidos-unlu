package sdypp.tp2.p2p;

public enum P2PMessageType {
	// List available resources
	LIST,
	// Upload new resource
	POST,
	// Request a resource
	GET,
	// A master notifies the replica master that a new resource is available
	PUBLISH,
	// Generic acknowledgement
	ACK,
	// Generic error
	ERROR
}
