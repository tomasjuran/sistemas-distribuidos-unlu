package sdypp.tp2.p2p;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import sdypp.generic.CheckPort;
import sdypp.generic.ConnectionMetadata;
import sdypp.tp2.p2p.peer.P2PPeer;

public class MainPeer {
	private static Logger logger = LoggerFactory.getLogger("Client");
	
	public static void main(String[] args) {
		ConnectionMetadata master = null;
		
		File configFile = new File("p2p", "masters.json");
		ObjectMapper mapper = new ObjectMapper();
		try {
			JsonNode masters = mapper.readTree(configFile).get("masters");
			// Random master chosen as Replica Master
			int random = (int) (Math.random() * masters.size());
			master = new ConnectionMetadata(
					masters.get(random).get("host").asText(),
					masters.get(random).get("port").asInt());
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		
		int port = CheckPort.GetAvailable(Integer.parseInt(args[1]));
		
		Path resourcesDir = Paths.get("p2p", "Cliente " + port);
		try {
			if (!Files.isDirectory(resourcesDir))
				Files.createDirectory(resourcesDir);
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		
		P2PPeer peer = new P2PPeer(
				logger,
				master,
				resourcesDir.toString(),
				new ConnectionMetadata(args[0], port));
		
		Scanner in = new Scanner(System.in);
		String input = "";
		String[] arguments = null;
		String command = "";
		
		try {
			while (true) {
			System.out.println(Menu());
			input = in.nextLine();
			arguments = input.split(" ", 2);
			command = arguments[0].toLowerCase();
			switch (command) {
			case "list":
				Map<String, String> list = peer.getResourcesList();
				System.out.println("Resource list:");
				for (String resource: list.keySet()) {
					System.out.println("\"" + resource + "\": available @ (" + list.get(resource) + ")");
				}
				break;
				
			case "update":
				peer.updateList();
				break;
				
			case "get":
				peer.getResource(arguments[1]);
				break;
				
			case "post":
				peer.postResource(arguments[1]);
				break;
			
			case "master":
				System.out.println(peer.masterString());
				break;
				
			default:
				System.out.println("Command was not understood \"" + input + "\"");
			}
			
			System.out.println("\nPress enter to continue...");
			in.nextLine();
			}
		} finally {
			in.close();
		}
	}
	
	public static String Menu() {
		return "Commands:\n"
				+ "list            - list available resources\n"
				+ "update          - ask Master the updated list\n"
				+ "get <resource>  - get a resource from the list\n"
				+ "post <resource> - post a resource\n"
				+ "master          - get Master's address";
	}
}
