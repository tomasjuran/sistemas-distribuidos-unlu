package sdypp.tp2.p2p;

import java.io.Serializable;

public class P2PMessage implements Serializable {
	private static final long serialVersionUID = 1L;
	private final P2PMessageType msgType;
	private final Object msg;
	
	public P2PMessage(P2PMessageType msgType, Object msg) {
		this.msgType = msgType;
		this.msg = msg;
	}

	public P2PMessageType getMsgType() {
		return msgType;
	}

	public Object getMsg() {
		return msg;
	}
	
	@Override
	public String toString() {
		String s = this.msgType.toString();
		s += (msg == null) ? "" : ": " + this.msg.toString();
		return  s;
	}
}
