package sdypp.tp2.p2p.peer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;

import sdypp.generic.ConnectionMetadata;
import sdypp.generic.Server;
import sdypp.generic.SocketWrapper;
import sdypp.tp2.p2p.P2PMessage;
import sdypp.tp2.p2p.P2PMessageType;

/**
 * Client that listens to peer connections for resources
 * and requests resources to Master
 * @author Martín Tomás Juran
 * @version 1.0.0 May 17, 2020
 */
public class P2PPeer {
	private Logger logger;
	
	private final SocketWrapper masterConnection;
	private final String resourceDir;
	private Server peerServer;
	private ConnectionMetadata peerServerMetadata;
	
	private Map<String, String> resourcesList = new HashMap<String, String>();
	
	public P2PPeer(Logger logger, ConnectionMetadata master, String resourceDir, ConnectionMetadata peerServerMetadata) {
		this.logger = logger;
		this.masterConnection = new SocketWrapper(this.logger, master.getHost(), master.getPort());
		logger.info("Conectado con " + masterString());
		this.resourceDir = resourceDir;
		this.peerServerMetadata = peerServerMetadata;
		ExecutorService serverThread = Executors.newSingleThreadExecutor();
		serverThread.execute(() -> {
			try {
				this.peerServer = new Server(this.logger, peerServerMetadata.getPort());
				this.peerServer.setThreadHandlerClass(P2PClientHandler.class);
				this.peerServer.listen();
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
	}
	
	public Map<String, String> getResourcesList() {
		Map<String, String> completeList = new HashMap<String, String>(this.resourcesList);
		for (String localResource: localList()) {
			completeList.put(localResource, "local");
		}
		return completeList;
	}
	
	/**
	 * Ask for resource list to Master
	 * @return a Map with {resource: location}
	 */
	@SuppressWarnings("unchecked")
	public void updateList() {
		if (!masterConnection.connectionIsAlive())
			return;
		masterConnection.send(new P2PMessage(P2PMessageType.LIST, null));
		logger.info("Asked " + masterString() + " for resource list");
		Object o = masterConnection.receive();
		if (o instanceof P2PMessage) {
			P2PMessage msg = (P2PMessage) o;
			if (msg.getMsgType() == P2PMessageType.LIST) {
				this.resourcesList = (Map<String, String>) msg.getMsg();
				logger.info("Updated resource list! (sent by " + masterString() + ")");
				return;
			}
			logger.error("Received message from " + masterString() + " but it was not understood."
					+ " Message was " + msg);
		}
		logger.error("Could not retrieve resource list from " + masterString());
	}
	
	public void postResource(String name) {
		Map<String, String> msg = new HashMap<String, String>();
		msg.put(name, peerServerMetadata.toString());
		logger.info("Posting resource \"" + name + "\"... Sending " + P2PMessageType.POST + " to " + masterString() + "...");
		masterConnection.send(new P2PMessage(P2PMessageType.POST, msg));
		logger.info(P2PMessageType.POST + " sent to " + masterString());
	}
	
	public boolean getResource(String name) {
		if (localList().contains(name)) {
			logger.info("Resource " + name + " is locally available");
			return true;
		}
		if (!resourcesList.containsKey(name)) {
			logger.info("Resource \"" + name + "\" not found in resource list. Is resource list updated?");
			return false;
		}
		SocketWrapper peer = getPeerConnection(resourcesList.get(name));
		logger.info("Requesting peer @ " + peer.getRemoteString() + " for resource \"" + name + "\"");
		peer.send(new P2PMessage(P2PMessageType.GET, name));
		logger.info("Sent message to peer " + peer.getRemoteString() + ". Awaiting response...");
		Object o = peer.receive();
		if (o instanceof P2PMessage) {
			P2PMessage msg = (P2PMessage) o;
			if (msg.getMsgType() == P2PMessageType.ACK) {
				saveResource(msg.getMsg());
				logger.info("Resource received from " + peer.getRemoteString());
				return true;
			}
			logger.error("Received message from " + peer.getRemoteString() + " while asking for resource \"" + name
					+ "\", but it was not understood. Message was " + msg);
		}
		logger.error("Could not retrieve resource \"" + name + "\" from " + peer.getRemoteString());
		return false;
	}
	
	
	/**
	 * Returns the list of resources locally available
	 */
	public Set<String> localList() {
		Set<String> list = new HashSet<String>();
		try (Stream<Path> walk = Files.walk(Paths.get(resourceDir))) {
			List<String> result = walk.filter(Files::isRegularFile)
					.map(x -> x.getFileName().toString())
					.collect(Collectors.toList());
			result.forEach(list::add);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	private void saveResource(Object o) {
		if (o instanceof String) {
			Path path = Paths.get(resourceDir, (String) o);
			try {
				Files.deleteIfExists(path);
				Files.createFile(path);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private SocketWrapper getPeerConnection(String location) {
		String[] peer = location.split(":");
		return new SocketWrapper(this.logger, peer[0], Integer.parseInt(peer[1]));
	}
	
	public String masterString() {
		return "Master (" + masterConnection.getRemoteString() + ")";
	}
}
