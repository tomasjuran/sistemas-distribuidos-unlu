package sdypp.tp2.p2p.peer;

import java.net.Socket;
import java.util.Map;

import org.slf4j.Logger;

import sdypp.generic.ThreadHandler;
import sdypp.tp2.p2p.P2PMessage;
import sdypp.tp2.p2p.P2PMessageType;

/**
 * Class that handles peer to peer connections
 * @author Martín Tomás Juran
 * @version 1.0.0 May 17, 2020
 */
public class P2PClientHandler extends ThreadHandler {

	public P2PClientHandler(Logger logger, Socket socket, Map<String, Object> args) {
		// Client socket is assigned in super()
		super(logger, socket, args);
	}

	@Override
	public void run() {
		while(connectionIsAlive()) {
			Object o = receive();
			if (o != null && o instanceof P2PMessage) {
				P2PMessage msg = (P2PMessage) o;
				switch(msg.getMsgType()) {
				case GET:
					handleGet(msg);
					break;
				default:
					logger.error("Received message from " + getRemoteString() +
							" but it was not understood. Message was \"" + msg + "\"");
					break;
				}
			}
		}
	}
		
	/**
	 * If resource requested exists, send it to peer.
	 * @param msg message received.
	 */
	private void handleGet(P2PMessage msg) {
		String name = (String) msg.getMsg();
		logger.info("Received " + P2PMessageType.GET + " from " + getRemoteString() + " asking for \"" + name + "\"");
		logger.info("Sending \"" + name + "\" to " + getRemoteString() + "...");
		Object resource = getResource((String) msg.getMsg());
		send(new P2PMessage(P2PMessageType.ACK, resource));
		logger.info("Sent \"" + name + "\" to " + getRemoteString());
	}

	private Object getResource(String name) {
		return name;
	}
}
