package sdypp.utils;

public class Matrix {
	public static int Convolution(int[][] matrix1, int[][] matrix2) {
		int result = 0;
		int lastRow = matrix1.length - 1;
		int lastCol = matrix1[0].length - 1;
		for (int row = 0; row <= lastRow; row++) {
			for (int col = 0; col <= lastCol; col++) {
				result += matrix1[lastRow - row][lastRow - col] * matrix2[row][col];
			}
		}
		return result;
	}
}
