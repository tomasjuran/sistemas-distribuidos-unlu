package sdypp.generic;

import java.net.Socket;
import java.util.Map;

import org.slf4j.Logger;

/**
 * @author Martín Tomás Juran
 * @version 1.0.0 Mar 28, 2018
 */
public abstract class ThreadHandler extends SocketWrapper implements Runnable {
	
	public ThreadHandler(Logger logger, Socket socket, Map<String, Object> args) {
		super(logger, socket);
	}
	
}
